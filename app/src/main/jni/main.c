#include "com_bhatworks_androidjni_MainActivity.h"

/*
 * Class:     com_bhatworks_androidjni_MainActivity
 * Method:    getText
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_bhatworks_androidjni_MainActivity_getText
  (JNIEnv *env, jobject obj)
  {
    return (*env)->NewStringUTF(env, "Hello world!!");
  }
